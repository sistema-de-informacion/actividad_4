﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WedApp.Servicios.Models;

namespace WedApp
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            servicioAutenticar oservicioAutenticar=new servicioAutenticar();
            usuarioLogin ousuarioLogin = oservicioAutenticar.RecuperarToken(TextBox1.Text.ToString(), TextBox2.Text.ToString());

            if (ousuarioLogin.token !=null) 
            {
                Session.Add("ousuarioLogin", ousuarioLogin);
                Response.Redirect("./WfGrupoArticulo.aspx");
            }
        }
    }
}