﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WedApp.Servicios;
using WedApp.Servicios.Models;

namespace WedApp
{
    public partial class CU_ListaGrupoArticulo : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!this.IsPostBack) 
            {
                CargarGrid();
            }

        }
        void CargarGrid() 
        {
            ServicioGrupoArticulo oservicioGrupoArticulo = new ServicioGrupoArticulo();
            rgvListaGrupoArticulo.DataSource = oservicioGrupoArticulo.GetList(((usuarioLogin)Session["ousuarioLogin"]).token);
            rgvListaGrupoArticulo .DataBind();

        }
    }
}