﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CU_ListaGrupoArticulo.ascx.cs" Inherits="WedApp.CU_ListaGrupoArticulo"%>
<asp:GridView ID="rgvListaGrupoArticulo" runat="server"
    AutoGenerateColumns="false">
    <Columns>
      <asp:BoundField DataField="IdGrupoArticulo" HeaderText="Id"/>
      <asp:BoundField DataField="NombreGrupoArticulo" HeaderText="Nombre"/>
      <asp:BoundField DataField="Abreviatura" HeaderText="Abreviatura"/>
      <asp:BoundField DataField="IdPadre" HeaderText="IdPadre"/>
      <asp:BoundField DataField="IdPartida" HeaderText="IdPartida"/>
      <asp:BoundField DataField="Nivel" HeaderText="Nivel"/>
      <asp:BoundField DataField="Sector" HeaderText="Sector"/>
      <asp:BoundField DataField="FechaRegistro" HeaderText="FechaRegistro"/>
      <asp:BoundField DataField="EstadoRegistro" HeaderText="EstadoRegistro "/>
    </Columns>
    </asp:GridView>

